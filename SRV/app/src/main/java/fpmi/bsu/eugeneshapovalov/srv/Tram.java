package fpmi.bsu.eugeneshapovalov.srv;

public class Tram {

    public String name;
    public Route route;
    public int speed;

    public int lastStationIndex;

    public Tram() {
        lastStationIndex = -1;
    }

    public Station getArrivalSensorMessage() {
        ++lastStationIndex;
        if (lastStationIndex != route.stations.length) {
            return route.stations[lastStationIndex];
        } else {
            lastStationIndex = 0;
            return route.stations[lastStationIndex];
        }
    }

    public Station getCurrentStation() {
        return route.stations[lastStationIndex];
    }
}
