package fpmi.bsu.eugeneshapovalov.srv;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    static final int FIRST_ROUTE_SPEED = 4000;
    static final int SECOND_ROUTE_SPEED = 5000;
    static final int THIRD_ROUTE_SPEED = 4500;
    static final int FOURTH_ROUTE_SPEED = 5500;

    static final String[] STATION_NAMES = {
            "First",
            "Second",
            "Third",
            "Fourth",
            "Fifth",
            "Sixth",
            "Seventh",
            "Eighth"
    };

    static final String[] ROUTE_NAMES = {
            "100",
            "101",
            "102",
            "103",
    };

    private TextToSpeech mTtsSomethingWentWrong;

    private TramSystem mTramSystem;
    private Handler mHandler;

    private TextView mTvStationFirst;
    private TextView mTvStationSecond;
    private TextView mTvStationThird;
    private TextView mTvStationFourth;
    private TextView mTvStationFifth;
    private TextView mTvStationSixth;
    private TextView mTvStationSeventh;
    private TextView mTvStationEighth;

    private ImageView mIvFirstTramStatus;
    private ImageView mIvSecondTramStatus;
    private ImageView mIvThirdTramStatus;
    private ImageView mIvFourthTramStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTvStationFirst = (TextView) findViewById(R.id.tv_main_station_1);
        mTvStationSecond = (TextView) findViewById(R.id.tv_main_station_2);
        mTvStationThird = (TextView) findViewById(R.id.tv_main_station_3);
        mTvStationFourth = (TextView) findViewById(R.id.tv_main_station_4);
        mTvStationFifth = (TextView) findViewById(R.id.tv_main_station_5);
        mTvStationSixth = (TextView) findViewById(R.id.tv_main_station_6);
        mTvStationSeventh = (TextView) findViewById(R.id.tv_main_station_7);
        mTvStationEighth = (TextView) findViewById(R.id.tv_main_station_8);

        mIvFirstTramStatus = (ImageView) findViewById(R.id.iv_main_first_tram_status);
        mIvSecondTramStatus = (ImageView) findViewById(R.id.iv_main_second_tram_status);
        mIvThirdTramStatus = (ImageView) findViewById(R.id.iv_main_third_tram_status);
        mIvFourthTramStatus = (ImageView) findViewById(R.id.iv_main_fourth_tram_status);

        mIvFirstTramStatus.setOnClickListener(this);
        mIvSecondTramStatus.setOnClickListener(this);
        mIvThirdTramStatus.setOnClickListener(this);
        mIvFourthTramStatus.setOnClickListener(this);

        mTramSystem = new TramSystem(this);
        mHandler = new Handler();

        Route route1 = new Route();
        route1.name = ROUTE_NAMES[0];
        route1.stations = new Station[]{
                new Station(STATION_NAMES[0]),
                new Station(STATION_NAMES[1]),
                new Station(STATION_NAMES[2]),
                new Station(STATION_NAMES[3])
        };

        Route route2 = new Route();
        route2.name = ROUTE_NAMES[1];
        route2.stations = new Station[]{
                new Station(STATION_NAMES[0]),
                new Station(STATION_NAMES[3]),
                new Station(STATION_NAMES[6]),
                new Station(STATION_NAMES[5])
        };

        Route route3 = new Route();
        route3.name = ROUTE_NAMES[2];
        route3.stations = new Station[]{
                new Station(STATION_NAMES[0]),
                new Station(STATION_NAMES[2]),
                new Station(STATION_NAMES[7]),
                new Station(STATION_NAMES[6])
        };

        Route route4 = new Route();
        route4.name = ROUTE_NAMES[3];
        route4.stations = new Station[]{
                new Station(STATION_NAMES[7]),
                new Station(STATION_NAMES[0]),
                new Station(STATION_NAMES[2]),
                new Station(STATION_NAMES[5])
        };

        Tram tram = new Tram();
        tram.route = route1;
        tram.name = "_1";
        tram.speed = FIRST_ROUTE_SPEED;

        TramRunnable tramRunnable = new TramRunnable(this, tram);
        mHandler.postDelayed(tramRunnable, tram.speed);

        Tram tram1 = new Tram();
        tram1.route = route2;
        tram1.name = "_1";
        tram1.speed = SECOND_ROUTE_SPEED;

        TramRunnable tramRunnable1 = new TramRunnable(this, tram1);
        mHandler.postDelayed(tramRunnable1, tram1.speed);

        Tram tram2 = new Tram();
        tram2.route = route3;
        tram2.name = "_1";
        tram2.speed = THIRD_ROUTE_SPEED;

        TramRunnable tramRunnable2 = new TramRunnable(this, tram2);
        mHandler.postDelayed(tramRunnable2, tram2.speed);

        Tram tram3 = new Tram();
        tram3.route = route4;
        tram3.name = "_1";
        tram3.speed = FOURTH_ROUTE_SPEED;

        TramRunnable tramRunnable3 = new TramRunnable(this, tram3);
        mHandler.postDelayed(tramRunnable3, tram3.speed);

        mTtsSomethingWentWrong = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    mTtsSomethingWentWrong.setLanguage(Locale.getDefault());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        String speakText = "";
        switch (v.getId()) {
            case R.id.iv_main_first_tram_status:
                speakText = "Tram one ";
                break;
            case R.id.iv_main_second_tram_status:
                speakText = "Tram second ";
                break;
            case R.id.iv_main_third_tram_status:
                speakText = "Tram third ";
                break;
            case R.id.iv_main_fourth_tram_status:
                speakText = "Tram fourth ";
                break;
        }

        if (v.isSelected()) {
            v.setSelected(false);
            speakText += "fixed";
        } else {
            v.setSelected(true);
            speakText += "crashed";
        }

        mTtsSomethingWentWrong.speak(speakText, TextToSpeech.QUEUE_FLUSH, null);
    }

    private static class TramRunnable implements Runnable {

        private Tram tram;

        private WeakReference<MainActivity> activity;

        public TramRunnable(MainActivity activity, Tram tram) {
            this.activity = new WeakReference<>(activity);
            this.tram = tram;
        }

        @Override
        public void run() {
            if (activity.get() == null) {
                return;
            }

            final ImageView ivTramStatus = activity.get().getIvStatus(tram.route.name);
            if (!ivTramStatus.isSelected()) {
                activity.get().mTramSystem.obtainMessage(TramSystem.MSG_TRAM_ARRIVAL_TO_STATION, tram)
                        .sendToTarget();
            }
            activity.get().mHandler.postDelayed(this, tram.speed);
        }
    }

    private static final class TramSystem extends Handler {

        private static final int MSG_TRAM_ARRIVAL_TO_STATION = 1;

        private WeakReference<MainActivity> activity;

        public TramSystem(MainActivity activity) {
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            if (activity.get() == null) {
                return;
            }

            switch (msg.what) {
                case MSG_TRAM_ARRIVAL_TO_STATION:
                    Tram tram = (Tram) msg.obj;
                    activity.get().updateStation(tram);
                    break;
            }
        }
    }

    private void updateStation(Tram tram) {
        int time;
        Station newStation = tram.getArrivalSensorMessage();
        Station[] stations = tram.route.stations;

        for (int i = 0; i < stations.length; ++i) {
            if (stations[i] == newStation) {
                TextView tv = getTvStation(stations[i].name);
                final String[] stationValues = tv.getText().toString().split(",");
                if (stationValues.length == 1) {
                    String firstRow = stationValues[0];
                    if (TextUtils.isEmpty(firstRow)) {
                        time = stations.length * tram.speed;
                        tv.setText(composeStationValue(tram, time) + " (Arrived).");
                    } else {
                        time = stations.length * tram.speed;
                        if (firstRow.contains(tram.route.name)) {
                            tv.setText(composeStationValue(tram, time) + " (Arrived).");
                        } else {
                            firstRow = firstRow.replace("\n", "");
                            tv.setText(firstRow + ",\n" + composeStationValue(tram, time) + " (Arrived)");
                        }
                    }
                } else {
                    boolean isStationFound = false;
                    for (int i1 = 0; i1 < stationValues.length; i1++) {
                        if (stationValues[i1].contains(tram.route.name + "_1")) {
                            isStationFound = true;
                            time = stations.length * tram.speed;
                            stationValues[i1] = composeStationValue(tram, time) + " (Arrived)";
                        } else {
                            stationValues[i1] = stationValues[i1].replace("\n", "");
                        }
                    }
                    String[] newStationValues = null;
                    if (!isStationFound) {
                        time = stations.length * tram.speed;
                        newStationValues = Arrays.copyOf(stationValues, stationValues.length + 1);
                        newStationValues[newStationValues.length - 1] = composeStationValue(tram, time) + " (Arrived)";
                    }
                    Arrays.sort(newStationValues != null ? newStationValues : stationValues, new StationTimeComparator());
                    tv.setText(TextUtils.join(",\n", newStationValues != null ? newStationValues : stationValues));
                }
            } else {
                TextView tv = getTvStation(stations[i].name);
                final String[] stationValues = tv.getText().toString().split(",");
                if (stationValues.length == 1) {
                    String firstRow = stationValues[0];
                    if (TextUtils.isEmpty(firstRow)) {
                        if (i - tram.lastStationIndex < 0) {
                            time = (i - tram.lastStationIndex + tram.route.stations.length) * tram.speed;
                        } else {
                            time = (i - tram.lastStationIndex) * tram.speed;
                        }
                        tv.setText(composeStationValue(tram, time));
                    } else {
                        if (i - tram.lastStationIndex < 0) {
                            time = (i - tram.lastStationIndex + tram.route.stations.length) * tram.speed;
                        } else {
                            time = (i - tram.lastStationIndex) * tram.speed;
                        }
                        if (firstRow.contains(tram.route.name)) {
                            tv.setText(composeStationValue(tram, time));
                        } else {
                            firstRow = firstRow.replace("\n", "");
                            tv.setText(firstRow + ",\n" + composeStationValue(tram, time));
                        }
                    }
                } else {
                    boolean isStationFound = false;
                    for (int i1 = 0; i1 < stationValues.length; i1++) {
                        if (stationValues[i1].contains(tram.route.name + "_1")) {
                            isStationFound = true;
                            if (i - tram.lastStationIndex < 0) {
                                time = (i - tram.lastStationIndex + tram.route.stations.length) * tram.speed;
                            } else {
                                time = (i - tram.lastStationIndex) * tram.speed;
                            }
                            stationValues[i1] = composeStationValue(tram, time);
                        } else {
                            stationValues[i1] = stationValues[i1].replace("\n", "");
                        }
                    }
                    String[] newStationValues = null;
                    if (!isStationFound) {
                        time = stations.length * tram.speed;
                        newStationValues = Arrays.copyOf(stationValues, stationValues.length + 1);
                        newStationValues[newStationValues.length - 1] = composeStationValue(tram, time);
                    }
                    Arrays.sort(newStationValues != null ? newStationValues : stationValues, new StationTimeComparator());
                    tv.setText(TextUtils.join(",\n", newStationValues != null ? newStationValues : stationValues));
                }
            }
        }
    }

    private static class StationTimeComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            int arrivedIndex1;
            int arrivedIndex2;

            if (o1.contains("(")) {
                arrivedIndex1 = o1.split(":")[1].indexOf("(") - 1;
            } else {
                arrivedIndex1 = o1.split(":")[1].length();
            }
            if (o2.contains("(")) {
                arrivedIndex2 = o2.split(":")[1].indexOf("(") - 1;
            } else {
                arrivedIndex2 = o2.split(":")[1].length();
            }

            int time1 = Integer.valueOf(o1.split(":")[1].substring(1, arrivedIndex1));
            int time2 = Integer.valueOf(o2.split(":")[1].substring(1, arrivedIndex2));

            return Integer.compare(time1, time2);
        }
    }

    private String composeStationValue(Tram tram, int time) {
        return tram.route.name + tram.name + " : " + time;
    }

    private TextView getTvStation(String stationName) {
        switch (stationName) {
            case "First":
                return mTvStationFirst;
            case "Second":
                return mTvStationSecond;
            case "Third":
                return mTvStationThird;
            case "Fourth":
                return mTvStationFourth;
            case "Fifth":
                return mTvStationFifth;
            case "Sixth":
                return mTvStationSixth;
            case "Seventh":
                return mTvStationSeventh;
            case "Eighth":
                return mTvStationEighth;
            default:
                return mTvStationFirst;
        }
    }

    @NonNull
    private ImageView getIvStatus(String routeName) {
        switch (routeName) {
            case "100":
                return mIvFirstTramStatus;
            case "101":
                return mIvSecondTramStatus;
            case "102":
                return mIvThirdTramStatus;
            case "103":
                return mIvFourthTramStatus;
            default:
                return mIvFirstTramStatus;
        }
    }

}
